#ifndef __SYMANTECAPP_H__
#define __SYMANTECAPP_H__

#include <iostream>
#include <map>

#include <im_error.h>
#include <extapi.hxx>

#include "symantecscan.hxx"
#include "syconstants.hxx"

using namespace std;

//namespace symantecscan
//{

class SymantecConfig
{
public:
	// construct config instance
	static SymantecConfig* getInstance()
	{
		LOG(Notification, "SymantecConfig:getInstance", "Enter");
		if(!_pSymantecConfig)
		{
			_pSymantecConfig = new SymantecConfig();
		}

		return _pSymantecConfig;
	}

	// release config instance
	static void releaseInstance()
	{
		if(_pSymantecConfig)
			delete _pSymantecConfig;
	}

	// getter and setters for symantec specific config keys
	const char* getSymantecInboundAction() { return _strInboundAction.c_str(); }
	const char* getSymantecOutboundAction() { return _strSymantecOutboundAction.c_str(); }
	const char* getSymantecServerConnection() { return _strSymantecServerConnection.c_str(); }
	const char* getSymAVNoticeBodySender() { return _strSymAVNoticeBodySender.c_str(); }
	const char* getSymAVNoticeHeaderSender() { return _strSymAVNoticeHeaderSender.c_str(); }

private:
	SymantecConfig()
	{
		LOG(Notification, "SymantecConfig::SymantecConfig", "Enter");	
		MxConfig* pMxConf = NULL;	
		try
		{	
			// SymantecAction key
			pMxConf = new MxConfig(KEY_SYMACTION, "Allow", "", EXTN_SERVICE, Cfg, CFG_SERV_RESTART);
			_strInboundAction 	= pMxConf->getString(KEY_SYMACTION, "Allow");
			//delete pMxConf;
			_arrSymantecConfg.push_back(pMxConf);

			// SymantecOutboundAction key
			//pMxConf = new MxConfig("SymantecOutboundAction", "Allow", "", EXTN_SERVICE, Cfg, CFG_SERV_RESTART);
			//_strSymantecOutboundAction = pMxConf->getString("SymantecOutboundAction", "Allow");
			//delete pMxConf;
			//_arrSymantecConfg.push_back(pMxConf);

			// SymantecTimeoutSecs key
			//pMxConf = new MxConfig("SymantecTimeoutSecs", "0", "", "SymantecScan", Cfg, CFG_SERV_RESTART);
			//_strInboundAction 	= pMxConf->getString("SymantecTimeoutSecs", "0");
			//delete pMxConf;
			
			// SymantecServerConnection key
			pMxConf = new MxConfig(KEY_SYMSERVERCONN, "server:0.0.0.0:1344", "", EXTN_SERVICE, Cfg, CFG_SERV_RESTART);
			_strSymantecServerConnection = pMxConf->getString(KEY_SYMSERVERCONN, "server:0.0.0.0:1344");
			//delete pMxConf;
			_arrSymantecConfg.push_back(pMxConf);

			// SymantecVirusNoticeBodySender key
			pMxConf = new MxConfig(KEY_SYMVRSNOTICEBODYSENDER, "Body", "", EXTN_SERVICE, Cfg, CFG_SERV_RESTART);
			_strSymAVNoticeBodySender = pMxConf->getString(KEY_SYMVRSNOTICEBODYSENDER, "Body");
			//delete pMxConf;
			_arrSymantecConfg.push_back(pMxConf);

			// SymantecVirusNoticeHeaderSender key
			pMxConf = new MxConfig(KEY_SYMVRSNOTICEHEADERSENDER, "Header", "", EXTN_SERVICE, Cfg, CFG_SERV_RESTART);
			_strSymAVNoticeHeaderSender = pMxConf->getString(KEY_SYMVRSNOTICEHEADERSENDER, "Header");
			//delete pMxConf;
			_arrSymantecConfg.push_back(pMxConf);	

			/*
			// SymantecTimeoutAction key
			pMxConf = new MxConfig("SymantecTimeoutAction", "Allow", "", "SymantecScan", Cfg, CFG_SERV_RESTART);
			_strSymantecTimeoutAction = pMxConf->getString("SymantecTimeoutAction", "Allow");
			delete pMxConf;

			// SymantecConnectionPoolSize key
			pMxConf = new MxConfig("SymantecConnectionPoolSize", 10, "", "SymantecScan", Cfg, CFG_SERV_RESTART);
			_strSymantecConnectionPoolSize = pMxConf->getString("SymantecConnectionPoolSize", 10);
			delete pMxConf;

			// SymantecAntiVirusNotice key
			pMxConf = new MxConfig("SymantecAntiVirusNotice", "a message has potentially unsafe content and hence can't process it!", "", "SymantecScan", Cfg, CFG_SERV_RESTART);
			_strSymantecAntiVirusNotice = pMxConf->getString("SymantecAntiVirusNotice", "a message has potentially unsafe content and hence can't process it!");
			delete pMxConf;

			// SymantecCOSAuthoritative key
			pMxConf = new MxConfig("SymantecCOSAuthoritative", False, "", "SymantecScan", Cfg, CFG_SERV_RESTART);
			_strSymantecCOSAuthoritative = pMxConf->getString("SymantecCOSAuthoritative", False);			
			delete pMxConf;

			// MailSymantecInboundVirusAction key
			pMxConf = new MxConfig("MailSymantecInboundVirusAction", "Allow", "", "SymantecScan", Cfg, CFG_SERV_RESTART);
			_strMailSymantecInboundVirusAction = pMxConf->getString("MailSymantecInboundVirusAction", "Allow");
			delete pMxConf;

			// MailSymantecOutboundVirusAction key
			pMxConf = new MxConfig("MailSymantecOutboundVirusAction", "Allow", "", "SymantecScan", Cfg, CFG_SERV_RESTART);
			_strMailSymantecOutboundVirusAction = pMxConf->getString("MailSymantecOutboundVirusAction", "Allow");	
			delete pMxConf;			
		*/	
		}
		catch(exception &ex)
		{
			LOG(Notification, "SymantecConfig::SymantecConfig Exception :", "%s", ex.what());
		}

		LOG(Notification, "SymantecConfig", "Exit");	
	}

	~SymantecConfig()
	{
		for(int i =0; i < _arrSymantecConfg.size(); i++)
			delete _arrSymantecConfg[i];
	}

	void _setInboundAction()
	{
		//_strInboundAction    = (new MxConfig())->getString("SymantecAction", "Allow");
	}

private:
	SymantecAction			_inboundAction;

	// Symantec client specific keys
	string 					_strInboundAction;	
	string					_strSymantecOutboundAction;
	string 					_strSymantecTimeoutSecs;
	string 					_strSymAVNoticeHeaderSender;
	string          		_strSymAVNoticeBodySender;
	string					_strSymantecServerConnection;

	//string 				_strSymantecTimeoutAction;
	//string				_strSymantecConnectionPoolSize;

	//string				_strSyamntecFailRetrySecs;
	//string				_strSyamntecServerReadWriteTime;
	//string				_strSymantecKeepAliveConnections;
	//string				_strSymantecAntiVirusNotice;

	//string				_strSymantecCOSAuthoritative;
	//string				_strMailSymantecInboundVirusAction;
	//string				_strMailSymantecOutboundVirusAction
	
	vector<MxConfig*>		_arrSymantecConfg;

	// Mx Specific config keys
	
	// single instance of the config
	static SymantecConfig* 	_pSymantecConfig;
};


//+---------------------------------------------------------------------------
// Synopsis: This is the first object created when symantecscan.so file loaded
//			by imextserv. It create log object, initialize SymantecScanner.
//			This object will be destructed when imextserv shut down.
//+---------------------------------------------------------------------------
class SymantecApplication
{
public:
	// This function should called before SymantecApplication constructor
	static void initStat();

	void Initialize();
	void Finalize();
	
	// Constructor
	SymantecApplication(const char* prgName);
	~SymantecApplication();

	// Getter for data members
	SymantecScanner* getSymantecScanner() { return _pSymantecScanner; }

	inline bool isCOSAuthoritative() { return _bCOSAuthoritative; }
	inline bool isAppInitialized() { return _bApplicationInitialized; }	
	inline bool isSenderNoticeEnabled() { return _bSenderNotice; }
	inline bool isRecipientNoticeEnabled() { return _bRecipientNotice; }
	inline bool isRemoteRecipientNoticeEnabled() { return _bRemoteRecipientNotice; }

	StringInfo* getDisposition(SymantecAction symAction);

	const char* getSymantecInboundAction() { return _pSymantecConfig->getSymantecInboundAction(); }
	const char* getSymantecOutboundAction() { return _pSymantecConfig->getSymantecOutboundAction(); }
	const char* getSymantecServerConnection() { return _pSymantecConfig->getSymantecServerConnection(); }
	const char* getSymAVNoticeHeaderSender() { return _pSymantecConfig->getSymAVNoticeHeaderSender(); }
	const char* getSymAVNoticeBodySender() { return _pSymantecConfig->getSymAVNoticeBodySender(); }
	SymantecAction	getSymantecAction();

public:
	struct Compare
	{
		bool operator() ( const string &s1, const string& s2) {
			if (s1.compare(s2) == 0) return true;
			else return false;
		}
	};

	typedef map<string, SymantecAction, Compare> SymantecActionList;
	typedef map<string, SymantecVirusNoticeCosType, Compare> SymantecVirusNoticeList;
	typedef map<SymantecAction, StringInfo> DispositionHash;

private:
	// Setters for data members
	void _createDispHashTable();
	void _createSymantecActionHashTable();
	//void _createSymantecVirusNoticeHashTable();
	
	void _addDisp(SymantecAction symAction, const char* disp);

private:
	// data members 
	bool 				_bApplicationInitialized;
	bool 				_bSenderNotice;
	bool 				_bRecipientNotice;
	bool 				_bRemoteRecipientNotice;
	bool 				_bCOSAuthoritative;
	string 				_strProgram;
	string 				_strHostName;

	// Symantec config object
	SymantecConfig* 	_pSymantecConfig;
	
	// Symantec AV scanner	
	SymantecScanner* 	_pSymantecScanner;
	DispositionHash 	_dispList;
	SymantecActionList 	_actionList;	 
}; 

extern SymantecApplication* symantecApp;

//}

#endif
