#ifndef __SY_CONSTANTS_HXX__
#define __SY_CONSTANTS_HXX__

#include "extapi.hxx"

// typedefs for user defined data type 
typedef unsigned char uchar;

// module name 
const char* const EXTN_SERVICE 	= "SymantecScan";
const char* const MODULE_NAME 	= "SymantecExtSvc";

// config key name
const char* const KEY_SYMACTION 				= "SymantecAction";
const char* const KEY_SYMSERVERCONN 			= "SymantecServerConnection";
const char* const KEY_SYMVRSNOTICEHEADERSENDER	= "SymantecVirusNoticeHeaderSender";
const char* const KEY_SYMVRSNOTICEBODYSENDER 	= "SymantecVirusNoticeBodySender";

// message constants
const int INITIAL_LOG_MSG_SIZE = 4096;

// Log severity
#define Notification 	IM_NOTIFICATION
#define Warning 		IM_WARNING
#define Error 			IM_ERROR
#define Urgent 			IM_URGENT
#define Fatal 			IM_FATAL

#define LOG SymantecLog::writeLog
#define DIAGC  SymantecLog::writeTrace

#define CFG static MxConfig 
#define CFG_SERV_RESTART IM_CFG_SERV_RESTART

typedef enum
{
	SymantecInbound		= 0,
	SymantecOutbound 	= 1
} SymantecHookType;

// Syamntec Scan return code
typedef enum
{
	SymantecSuccess		= 0,
	SymantecFailure		= 1,
	SymantecNotCleaned	= 2,	// Symantec Engine identify a varus in a message 
								// but could not clean it
	SymantecNotScanned	= 3,
	SymantecCleanMail	= 4,
	SymantecTimeout		= 5	
} SymantecCode;

// Symantec extension service option on unsafe mail
typedef enum
{
	SymantecActionNull =1,
	SymantecActionAllow,		// Allow viruses to be delivered
	SymantecActionSideline,		// Sideline mail that has viruses 
	SymantecActionAVSpool,		// Move the mails to sidelined/avspool director
	SymantecActionDiscard,		// Discard mails that have viruses
	SymantecActionReject,		// Reject mails that have viruses
	SymantecActionDefer,		// Defer message
	SymantecActionRepair,		// Repair mails that have viruses
	SymantecActionClean,		// Clean mails that have viruses
	SymantecActionInvalid
} SymantecAction;

// Symantec extension service action basis on COS or system configuration
typedef enum
{
	SymantecUnknown,
	SymantecInboundCos,			// Using Recipients CoS value
	SymantecInboundConfig,		// Using Config key, Symantec (Inbound) Action value
	SymantecOutboundCos,		// Using Sender's CoS value
	SymantecOutboundConfig		// Using Config key, symantecOutboundAction value
} SymantecActionBase;

// Symantec message scan verdict
typedef enum
{
	SymantecVerdictOkey,				// Mail is clean
	SymantecVerdictSuspect,				// Mail is suspect of having a virus
	SymantecVerdictSuspectRepaired,		// Mail is infected with virus and got repaired
	SymantecVerdictSuspectNoRepaired,	// Mail is infected with virus and not repaired	
	SymantecVerdictBadMsg,				// Message was infected. It could not be cleaned.
	SymantecVerdictTimeout,				// Symantec Protection Engine timed out
	SymantecVerdictFailed,				// Symantec Protection Engine failed to return verdict
	SymantecVerdictUnknown
} SymantecVerdict;

// 
typedef enum 
{
	SymantecVirusNoticeNull,
	SymantecVirusNoticeInvalid,
	SymantecVirusNoticeNone,
	SymantecVirusNoticeCosOwner
} SymantecVirusNoticeCosType;

// Configuration specific constant
typedef enum
{
	Cfg,
	CfgInt,
	CfgIntBound,
	CfgBool,
	CfgTime
} ConfigType;

#endif

