#ifndef __SYMANTECSCAN_H__
#define __SYMANTECSCAN_H__

#include <string.h>

#include "symcsapi.h"
#include "syconstants.hxx"
#include "msginfo.hxx"

//namespace symantecscan
//{
//+---------------------------------------------------------------------------
//
// Synopsis: Scan and clean the mail for viruses. It uses Symantec ICAP library
//			to send data to Symatec Engine for scanning.
//---------------------------------------------------------------------------
class SymantecScanner
{
public:
	// Constructor and Destructor
	SymantecScanner();
	~SymantecScanner();

	// Main function to scan and clean message
	SymantecVerdict scanMessage(MsgInfo*, bool noLogging = false);
	
	//SymantecVerdict cleanMessage(MsgInfo*, 
	void getScanProblemInfo(HSCANRESULTS hResults, int iWhichProblem, string virusName);
	bool isValid() { return _initialized; }

private:
	SymantecVerdict _doScanning(const string strContent, MsgInfo* msg, string virusName);
	HSCANCLIENT 	_scanClient;
	bool 			_initialized;
};

//}

#endif

