#ifndef __MSGINFO_H__
#define __MSGINFO_H__

#include <iostream>
#include <string>

#include "syconstants.hxx"
#include "syutils.hxx"

#include <extapi.hxx>

using std::string;

//namespace symantecscan
//{

class MailUser
{
public:
	MailUser(IMxMailgram* Mailgram);
	virtual ~MailUser();	
	
	const char*					getAddress();
	const StringInfo*			getAddrStrInfo();
	virtual SymantecAction		getSymantecAction() = 0;
	inline SymantecActionBase	getSymantecActionBase() { return _actionType; }
	inline void 				setSymantecActionBase(SymantecActionBase symAction) { _actionType = symAction; }
	void						setSymantecAction(SymantecAction symAction);	

protected:
	IMxMailgram*      		_mailgram;
	IMxEnvelope*			_envelope;
	SymantecActionBase		_actionType;
	SymantecAction			_action;
	string					_noticeStr;
	int						_result;
};


class Recipient : public MailUser
{
public:
	Recipient(IMxMailgram* Mailgram, int id);
	~Recipient();

	const char* 		getAddress();
	const StringInfo* 	getAddrStrInfo();
	SymantecAction		getSymantecAction();
	
	const char* 		getRcptToDisp();
	const char* 		getRcptToAttr();

	int 				getDisposition(StringInfo** infoRcptToDisp);
	bool				isRemoteAddress();
	void 				setRemoteAddress(bool bRemote) { _bRemote = bRemote; }

private:
	int 	_index;
	bool	_bAddress;
	bool 	_bRemote;
};


class Sender : public MailUser
{
public:
	Sender(IMxMailgram* Mailgram);
	~Sender();

	char const* 		getAddress();
	const StringInfo* 	getAddrStrInfo();
	SymantecAction 		getSymantecAction();

	char const* 		getMailFromAttr();
};


class MsgInfo
{
public:
	MsgInfo(IMxMailDelivery* MailDelivery);
	~MsgInfo(); 

	// getters and setters
	const StringInfo* 	getBody();
	const StringInfo* 	getHeader();
	const StringInfo* 	getMailFrom();

	string 				getSubject();
	string 				getMessageID();
	const StringInfo* 	getNewBody()
	{	return (_newBody != NULL ? _newBody : getBody()); }

	const StringInfo* 	getNewHeader()
	{ 	return (_newHeader != NULL ? _newHeader : getHeader()); }

	void 				setMessageID();	
	int					getRecipCount() { return _rcptToCount; }
	
	MxString 			readFromHeader(const char*, const char*, int len=0);
	MxString            readFromHeader(const char*, bool decode=true, bool perl=false);

	string 				readFromBody(const char*, const char*, int len=0); 
	bool				isRepaired() { return _bRepaired; }
	void 				setRepaired(bool flag) { _bRepaired = flag; }

	// Get mail attribute
	bool 				isOutboundMail() { return _outboundMail; }
	const char* 		getVirusName() { return _virusname.c_str(); }
	void 				setVirusName(const char* name, bool scanSet=false);
	SymantecVerdict 	getSymantecVerdict() { return _virusVerdict; }
	void 				setSymantecVerdict(SymantecVerdict verdict, bool scanSet=false);
	SymantecActionBase 	getSymantecActionType() { return _symantecActionType; }
	void 				setSymantecActionType(SymantecActionBase symActionType, SymantecAction symAction);
	
	// Get Sender and Recipients
	Recipient* 			getRecipient(int index);
	const char* 		getRecipientList();
	Sender* 			getSender() { return _MailSender; }
	
private:	
	SymantecCode		_storeSender(void);
	SymantecCode		_storeRecipients(void);
	void				_cleanRecipArray(void);
	MxString			_searchSString(const MxString&, const char*, const char*);

public:
	typedef vector<Recipient *> RecipArray;

private:
	int 				_result;
	string				_virusname;
	bool				_bRepaired;
	SymantecActionBase 	_symantecActionType;
	SymantecAction		_symantecAction;
	SymantecVerdict		_virusVerdict;


	string 				_msgID;
	string 				_subject;
	bool				_outboundMail;
	unsigned int		_rcptToCount;
	RecipArray			_recipArray;
	MxString			_recipientList;
	StringInfo*			_newHeader;
	StringInfo*			_newBody;
	Sender*				_MailSender;
	MxString			_dateSubmitted;
	MxString			_peerIP;

	IMxMailDelivery*	_mailDelivery;
	IMxMailgram*		_mailgram;
	IMxEnvelope*		_envelope;
	IMxMailMessage*		_mailMessage;
};

MxString escape(const MxString& line, bool perl = true);
//}
#endif
