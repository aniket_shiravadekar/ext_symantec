
#include <stdio.h>
#include <iostream>
#include <extapi.hxx>

#include "syutils.hxx"
#include "syconstants.hxx"
#include "symantechandler.hxx"

using namespace std;
//using namespace	symantecscan;

// Re-declared APIUserMsg because APIUserMsgs.hxx lives in mercury/omu
// and we can't include omu header files here.
#define APIUserMsg      0x1e0001

// This object is used to initialize Symantec logging and tracing
SymantecLog symantecLog(MODULE_NAME);

SymantecApplication* symantecApp = NULL; 

extern SymantecCode ProcessMail(IMxMailDelivery*, SymantecHookType);


//+---------------------------------------------------------------------
// Initialize global application object
//+---------------------------------------------------------------------
SVCAPI void Initialize( void )
{
	symantecLog.initSymantecLogging();
	symantecLog.setMainLogEvent("APIUserMsg", APIUserMsg);
	LOG(Notification, "Initialize", "Enter");
 
	try {
		DIAGC("symantecscan", 1, "Entering Initialize");
		symantecApp = new SymantecApplication(EXTN_SERVICE);

		// Initialize symantec scan application
		symantecApp->Initialize();

		if (!symantecApp->isAppInitialized()) throw("Initialization failed");

	} catch(exception &ex) {
		LOG(Fatal, "AVSymantecInitFailed", 
			"Symantec Extension Service initialization failed. All mail will be deffered");
		LOG(Fatal, "Initialize exception", "%s", ex.what());
		return;
	} catch(...) {
		LOG(Fatal, "AVSymantecInitFailed", 
			"Symantec Extension Service initialization failed. All mail will be deffered");
		return;	
	}

	// Log the version info and initialization
	LOG(Notification, "SymantecExtSvc", "Sucessfully initialized Symantec extensions service");
}

//+---------------------------------------------------------------------
// Clean up and shutdown the application.
//+---------------------------------------------------------------------
SVCAPI void Finalize( void )
{
	//Free Symantec Logging
	symantecLog.freeSymantecLogging();

	LOG(Notification, "Finaalize S", "Entering Finalize");
	DIAGC("symantec", 1, "Calling Finalize");
	symantecApp->Finalize();
	delete symantecApp;	
}


SVCAPI const AdvertiseInfo* Advertise( void )
{
	static EventInfo SymantecLocalEvents[] =
	{
		{ "SymantecLocal", MTA_LOCAL_DELIVER },
		0
	};

	static EventInfo SymantecRemoteEvents[] =
	{
		{ "SymantecRemote", MTA_REMOTE_DELIVER },
		0
	};

	static EventInfo SymantecEvents[] =
	{
		{ "SymantecLocal", MTA_LOCAL_DELIVER },
		{ "SymantecRemote", MTA_REMOTE_DELIVER },
		0
	};

	static ServiceInfo Service[] =
	{
		{ "Symantec", SymantecEvents },
		{ "SymantecLocal", SymantecLocalEvents },
		{ "SymantecRemote", SymantecRemoteEvents },
		0
	};

	static AdvertiseInfo info =
	{
		AdvertiseVersion1,
		(const ServiceInfo*) Service
	};

	return &info;
}


extern "C" SVCAPI void SymantecLocal ( IMxMailDelivery* mailDelivery );
extern "C" SVCAPI void SymantecRemote ( IMxMailDelivery* mailDelivery );


//+---------------------------------------------------------------------------
// Synopsis: Extension server call this function for every inbound mail. 
//			This function is executed in a separate thread.
//+---------------------------------------------------------------------------
void SymantecLocal( IMxMailDelivery* mailDelivery )
{
	LOG(Notification, "SymantecLocal", "Entering in SymantecLocal");
	ProcessMail(mailDelivery, SymantecInbound);	
	DIAGC("symantec", 5, "DONE processing MtaLocalDeliver mail");
} 


//+---------------------------------------------------------------------------
// Synopsis: Extension server call this function for every outbound mail.
//			This function is executed in a separate thread.
//+--------------------------------------------------------------------------- 
void SymantecRemote ( IMxMailDelivery* mailDelivery )
{
	LOG(Notification, "SymantecRemote", "Entering in SymantecRemote");
	ProcessMail(mailDelivery, SymantecOutbound);	
	DIAGC("symantec", 5, "DONE processing MtaRemoteDeliver mail");
}

//namespace symantecscan
//{
SymantecCode ProcessMail(IMxMailDelivery* mailDelivery, SymantecHookType hookType)
{
	LOG(Notification, "ProcessMail", "Enter");

	SymantecCode retCode;

	// check if Symantec Anti-Virus scanner initialized
	if(!symantecApp->isAppInitialized())
	{
		LOG(Urgent, "SymantecScanNotAvail", "Symantec App failed to initialized !");
		return SymantecFailure;
	}

	// Read the data from extension server and scan mail for virus 
	// If mail suspect then handle it as per action specified
	Handler handler(mailDelivery);

	try
	{
		if(handler.Initialize(hookType) != SymantecSuccess) {
			LOG(Error, "AVExtServerDataReadFailed", "Mail Deferred");
				 
			handler.deferMessage();
			return SymantecFailure;
		}

		retCode = handler.PreProcess();

		retCode = handler.Process();
		switch( retCode)
		{
			case SymantecCleanMail:
				handler.commitNoChange();
				return retCode;
			case SymantecTimeout:
				//handler.commitTimeout();
				return retCode;
			case SymantecNotCleaned:
				handler.takeBadMsgAction();
				return retCode;
			case SymantecFailure:
				handler.deferMessage();
				return retCode;
			default:
				// continue
				break;
		}

		if( handler.Commit() != SymantecSuccess ) {
			handler.deferMessage();
			return retCode;
		}
	}
	catch(exception& ex)
	{
		LOG(Urgent, "AVExtSvcProcessingFailed", "Exception: %s", ex.what());
		handler.deferMessage();
		return SymantecFailure;
	}
	catch(...)
	{
		LOG(Urgent, "ProcessMail", "Exception");
		handler.deferMessage();
		return SymantecFailure;
	}

	return SymantecSuccess;
}

//}	
