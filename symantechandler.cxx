#include <iostream>
#include "syconstants.hxx"
#include "symantechandler.hxx"

using namespace std;
 
//namespace symantecscan
//{


Handler::Handler(IMxMailDelivery* MailDelivery) 
{
	LOG(Notification, "Handler", "Entering");

	DIAGC("SymantecHandler", 5, "Handler Constructor");

	_mailDelivery = MailDelivery;
	_mailActionArray.clear();
	_virusNoticeRecipArray.clear();
	_virusNoticeCosRecipArray.clear();

	LOG(Notification, "Handler ctr", "Exit");
}

Handler::~Handler()
{
	if(_msgInfo)
		delete _msgInfo;
	
	//_cleanMainInfoArray();
}

//+---------------------------------------------------------------------------
//
// Method:		Handler::_cleanMainInfoArray
//
// Synopsis:	Loop through each element in MailInfoArray and
//				delete the objects.
//
//----------------------------------------------------------------------------
void Handler::_cleanMainInfoArray()
{
	for (MailInfoArray::iterator pAction = _mailActionArray.begin();
		pAction != _mailActionArray.end(); pAction++)
	{
		RecipInfoArray pInfo = pAction->second.recipInfoArray;
		for (int index = 0; index < int_cast(pInfo.size()); index++)
		{
			RecipInfo* recipInfo = pInfo[index];
			if (recipInfo == NULL)
				continue;
			
			delete recipInfo;
			recipInfo = NULL;
		}
	}
}


//+---------------------------------------------------------------------------
//
// Method: Handler::Initialize
// 
// Synopsis: Create MsgInfo object, read and store all the mail information
//			 from extension server.
//
//----------------------------------------------------------------------------
SymantecCode Handler::Initialize(SymantecHookType hookType)
{
	LOG(Notification, "Handler-Initialize", "Enter");
	DIAGC("SyamntecHandler", 5, "Entering Handler::Initialize");

	try
	{
		_msgInfo = new MsgInfo(_mailDelivery);
	}
 	catch(exception &ex)
	{
		LOG(Notification, "Handler::Initialize Exception", ex.what());	
		LOG(Error, "AVExtServerDataReadFailed", "Creating MsgInfo object");
		throw;
	}
	catch(...)
	{
		LOG(Error, "AVExtServerDataReadFailed", "Creating MsgInfo object");
		throw;
	}
	
	LOG(Notification, "Handler init", "Exit");
	return SymantecSuccess;
}



//+---------------------------------------------------------------------------
//
//	Method:		Handler::PreProcess
//
//	Synopsis:	Determine mail should be cleaned or scannedn 
//
//----------------------------------------------------------------------------
SymantecCode Handler::PreProcess()
{
	return SymantecSuccess;
}


SymantecCode Handler::Process()
{
	LOG(Notification, "Handler Process", "In Handler::Process");
	DIAGC("SymnatecHandler", 5, "Entering Handler::Process");

	SymantecVerdict verdict;

	verdict = symantecApp->getSymantecScanner()->scanMessage(_msgInfo, true);

	switch ( verdict )
	{
		case SymantecVerdictOkey:
			_postProcess();
			return SymantecCleanMail;
		case SymantecVerdictSuspectNoRepaired:
		case SymantecVerdictSuspectRepaired:
			_postProcess();
			return SymantecSuccess;
		case SymantecVerdictTimeout:
			return SymantecTimeout;
		case SymantecVerdictBadMsg:
			return SymantecNotCleaned;
		case SymantecVerdictUnknown:
		case SymantecVerdictFailed:
		default:
			return SymantecFailure;
	}

	return SymantecSuccess;
}

//+---------------------------------------------------------------------------
//
// Synopsis: commit mail with defer disposition for all recipient.
//
//---------------------------------------------------------------------------
void Handler::deferMessage()
{
	_addMailInfo(SymantecActionInvalid);
	commitResponse();
}

void Handler::takeBadMsgAction()
{	
	SymantecAction symAction = SymantecActionReject;
	_addMailInfo(symAction);
	commitResponse();
}


SymantecCode Handler::Commit()
{
	LOG(Notification, "Handler::Commit", "Entry");

	commitResponse();

	//for(int index=0; index < _recipCount; index++)
	//{
	//	Recipient* recip = _msgInfo->getRecipient(index);
	//	if ( recip->isRemoteAddress()) {

	// checking sender information
	Sender* sender = _msgInfo->getSender();
	
	// generate notice
	generateNotice(symantecApp->getSymAVNoticeHeaderSender(), 
					symantecApp->getSymAVNoticeBodySender(), true);

	LOG(Notification, "Handler::Commit", "Exit");
	return SymantecSuccess;
}

//+---------------------------------------------------------------------------
//
// Synopsis:	Add a record for each for 1. Clean, 2. Reject, 3. Sideline
//				4. Discard, Invalid and Allow.
//				
//				If the recip is NULL then all the recipient are scanned and
//				RcptTo and RcptToDisp will be created.
//
//----------------------------------------------------------------------------
void Handler::_addMailInfo(SymantecAction symAction, Recipient* recp)
{
	LOG(Notification, "Handler::_addMailInfo", "Entry");

	StringInfo* newHeader = NULL;	
	StringInfo* newBody = NULL;
	ActionKey key;

	switch ( symAction )
	{
		case SymantecActionReject:
			// New Header and old body
			// The cleaned msg body is stored in Handler
			newHeader = (StringInfo*) _msgInfo->getNewHeader();
			key = ActionNewHeader;
			LOG(Notification, "Handler::_addMailInfo", "Action:Reject");
			break;
		case SymantecActionRepair:
		case SymantecActionClean:
			// Old Header and New Body
			// The cleaned msg body is stored in Handler
			//newBody = (StringInfo*) _msgInfo->getCleanedBody();
			key = ActionNewBody;
			break;
		case SymantecActionSideline:
			key = ActionDefault;
		default:
			break;
	}

	StringInfo* rcptToDisp ; //= symantecApp->getDisposition(symAction);
	
	// check if record exist
	if (_mailActionArray.count(key) == 0) {
		// Create new record for action
		MailInfo mailInfo;
		mailInfo.header = NULL;
		mailInfo.body = NULL;
		_mailActionArray[key] = mailInfo;
	}

	_mailActionArray[key].header = newHeader;
	_mailActionArray[key].body = newBody;

	rcptToDisp = symantecApp->getDisposition(symAction);
	LOG(Notification, "_addMailInfo rcptToDisp ->", rcptToDisp->str);

	if(recp)
	{
		RecipInfo* recipInfo = _createRecipInfo(rcptToDisp, recp); 
		_mailActionArray[key].recipInfoArray.push_back(recipInfo);

		// add record for virus notice
		_addVirusNoticeRecipInfo(recipInfo, recp, symAction);
		return;	
	}

	for ( int index=0; index < _recipCount; index++)
	{
		Recipient* recip = _msgInfo->getRecipient(index);
		RecipInfo* recipInfo = _createRecipInfo(rcptToDisp, recip);
		_mailActionArray[key].recipInfoArray.push_back(recipInfo);
		
		// Add a record for virus notices
		_addVirusNoticeRecipInfo(recipInfo, recip, symAction);
	}

	_virusNoticeCount = _recipCount;
	LOG(Notification, "Handler::_addMailInfo", "Exit");
}


Handler::RecipInfo* Handler::_createRecipInfo(StringInfo* disp, Recipient* recip)
{
	LOG(Notification, "Handler::_createRecipInfo", "Entry");
	
	RecipInfo* recipInfo = new RecipInfo;
	recipInfo->rcptTo = (StringInfo*) recip->getAddrStrInfo();

	recipInfo->rcptToDisp  = disp;

	return recipInfo;
}


void Handler::_addVirusNoticeRecipInfo(RecipInfo* recipInfo, Recipient* recip, SymantecAction symAction)
{
	LOG(Notification, "Handler::_addVirusNoticeRecipInfo", "Entry");
	_virusNoticeRecipArray.push_back(recipInfo);

	_strVNoticeRecipients += recipInfo->rcptTo->str;
	LOG(Notification, "Handler::_addVirusNoticeRecipInfo strVNotice ->", "%s", _strVNoticeRecipients.c_str());
	LOG(Notification, "Handler::_addVirusNoticeRecipInfo", "Exit");
}


void Handler::_postProcess()
{
	LOG(Notification, "Handler::_postProcess", "Entry");

	//_bRemoteRecipNotice = symantecApp->isVirusNoticeRemoteRecipient();
	//_bLocalRecipNotice = symantecApp->isVirusNoticeRecipient();

	//SymantecAction senderAction = _msgInfo->getSender()->getSymantecAction();
	//SymantecActionBase senderActionType = _msgInfo->getSender()->getSymantecActionBase();

	// Loop through all recipients and add record for each recipient
	for(int index=0; index < _recipCount; index++)
	{
		Recipient* recip = _msgInfo->getRecipient(index);
		SymantecAction virusAction;
		SymantecActionBase symActionType;	

		//COS
		//virusAction = recip->getSymantecAction();
		//symActionType = recip->getSymantecActionBase();

		//_msgInfo->setSymantecActionType(symActionType, virusAction);

		virusAction = symantecApp->getSymantecAction();
		symActionType = SymantecInboundConfig; 
		_msgInfo->setSymantecActionType(symActionType, virusAction);

		// Create recipient and disposition array
		_addMailInfo(virusAction, recip);	
	}

	LOG(Notification, "Handler::_postProcess", "Exit");
} 


void Handler::commitResponse()
{
	for(MailInfoArray::iterator pAction = _mailActionArray.begin();
		pAction != _mailActionArray.end(); pAction++)
	{
		RecipInfoArray pInfo = pAction->second.recipInfoArray;

		if(pInfo.empty())
			continue;

		unsigned int count = (pInfo.size());
		StringInfo** infoRcptTo	= NULL;
		StringInfo** infoRcptToDisp = NULL;
		
		infoRcptTo = new StringInfo*[count];
		infoRcptToDisp = new StringInfo*[count];

		for(int index=0; index < (pInfo.size()); index++)
		{
			infoRcptTo[index] = pInfo[index]->rcptTo;
			infoRcptToDisp[index] = pInfo[index]->rcptToDisp;
		}

		// Group all the recipients and disposition and do one commit
		bool commitResult = _mailDelivery->commitResponse(0,
															count,
															(const StringInfo**) infoRcptTo,
															(const StringInfo**) infoRcptToDisp,
															(const StringInfo*) pAction->second.header,
															(const StringInfo*) pAction->second.body);
		delete[] infoRcptTo;
		delete[] infoRcptToDisp;
	}
}


void Handler::commitNoChange()
{
	_mailDelivery->commitResponse(0,0,0,0,0,0);
}


bool Handler::generateNotice(const char* header, 
							const char* body,
							bool bSender)
{
	LOG(Notification, "Handler::generateNotice", "Entry");

	MxString strHeader(header);
	MxString strBody( body);
	string strFrom(_msgInfo->getMailFrom()->str);
	string strNoAngleSender = strFrom.substr(1, strFrom.length()-2);

	strHeader.S("<Sender>",      strNoAngleSender.c_str(), "g");
	strHeader.S("<Virusname>",   _msgInfo->getVirusName(), "g");
	strHeader.S("<Date>",        "11-NOV-2016", "g");
	strHeader.S("<Postmaster>",  "sysadmin", "g");
	strHeader.S("<Message-ID>",  _msgInfo->getMessageID().c_str(), "g");
	strHeader.S("<Subject>",  _msgInfo->getSubject().c_str(), "g");


	if (bSender)
		strHeader.S("<Recipients>",  _msgInfo->getRecipientList(), "g");

	strHeader.S("\n", "\r\n", "g");
	strHeader += "\r\n";		

	LOG(Notification, "generateNotice Header ->", "%s", strHeader.AsPtr()); 
	
	strBody.S("<Sender>",      strNoAngleSender.c_str(), "g");
	strBody.S("<Virusname>",   _msgInfo->getVirusName(), "g");
	strBody.S("<Date>",        "11-NOV-2016", "g");
	strBody.S("<Postmaster>",  "sysadmin", "g");
	strBody.S("<Message-ID>",  _msgInfo->getMessageID().c_str(), "g");
	strBody.S("<Subject>",  _msgInfo->getSubject().c_str(), "g");

	if (bSender)
		strBody.S("<Recipients>",  _msgInfo->getRecipientList(), "g");

	strBody.S("\n", "\r\n", "g");
	strBody += "\r\n";

	LOG(Notification, "generateNotice Body ->", "%s", strBody.AsPtr());

	StringInfo newMailFrom, newHeader, newBody;
	newMailFrom.str = "sysadm"; //symantecApp->getVirusNoticeFrom();
	newMailFrom.len = 5; //strlen(symantecApp->getVirusNoticeFrom());
	newHeader.str = strHeader.AsPtr();
	newHeader.len = Length(strHeader);
	newBody.str   = strBody.AsPtr();
	newBody.len   = Length(strBody);

	StringInfo** strDispositions 	= NULL;
	StringInfo** strRecipients   	= NULL;
	int count						= 0;
	// commit
	string strMsg;
	string strUsers;

	if (bSender)
	{
		count =1;
		strDispositions = new StringInfo* [count];
		strRecipients 	= new StringInfo* [count];
	
		strDispositions[0] = symantecApp->getDisposition(SymantecActionAllow);
		strRecipients[0] = (StringInfo*) _msgInfo->getMailFrom(); 
		strMsg.assign("Sender");	
		strUsers.assign(_msgInfo->getMailFrom()->str);
	}

	LOG(Notification, "Generate Notice ", "Before");
	_mailDelivery->commitResponse((const StringInfo*) &newMailFrom,
										count,
										(const StringInfo**) strRecipients,
										(const StringInfo**) strDispositions,
										&newHeader,
										&newBody);
	LOG(Notification, "Generate Notice", "After");

	if (strDispositions != NULL )
		delete[] strDispositions;

	if (strRecipients != NULL)
		delete[] strRecipients;

	return true;
}

//}
