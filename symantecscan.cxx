#include <iostream>
#include <string>

#include "syconstants.hxx"
#include "symantecscan.hxx"
#include "symantecapp.hxx"

using namespace std;

//namespace symantecscan
//{

SymantecScanner::SymantecScanner()
{
	LOG(Notification, "SymantecScanner::SymantecScanner", "Enter");
	SC_ERROR retCode = 0;
	string strConnection (symantecApp->getSymantecServerConnection());
	_scanClient = NULL;
	_initialized = false;

	try
	{
		LOG(Notification, "SymantecScanner - Connection String:", "%s", strConnection.c_str());
		// Initialize symantec client
		retCode = ScanClientStartUp(&_scanClient, (char*)strConnection.c_str());

		if ( retCode == SC_OK )
			_initialized = true;

		LOG(Notification, "SymantecScanner- ScanClientStartUp Return :", "%d", retCode);
	}
	catch(exception &ex)
	{
		LOG(Notification, "SymantecScanner", "%s", ex.what());
	}
	catch(...)
	{
		LOG(Notification, "SymantecScanner", "generic exception");
	}

	LOG(Notification, "SymantecScanner::SymantecScanner", "Exit");
}


SymantecScanner::~SymantecScanner()
{
	LOG(Notification, "SymantecScanner", "Before ScanClientShutDown");
	ScanClientShutDown( _scanClient );
}


SymantecVerdict SymantecScanner::scanMessage(MsgInfo* msg, bool noLogging)
{
	LOG(Notification, "SymantecScanner", "SymantecScanner::scanMessage");
	SymantecVerdict retCode;
	string virusName;

	// Scan message header	
	retCode = _doScanning(msg->getHeader()->str, msg, virusName);
	LOG(Notification, "SymantecScanner::scanMessage HEADER SCAN Return : ", "%d", retCode);
	
	// Scan message body
	retCode = _doScanning(msg->getBody()->str, msg, virusName);
	LOG(Notification, "SymantecScanner::scanMessage MESSAGE BODY SCAN Return : ", "%d", retCode);

	// Set scan verdict
	msg->setSymantecVerdict(retCode, true);	

	return retCode;
}


SymantecVerdict SymantecScanner::_doScanning(string strContent, MsgInfo* msg, string virusName)
{
	SC_ERROR retCode;
	char* pOrgName = "msg.txt";
	HSCANSTREAM hScanStream = NULL;
	//SCSCANFILE_RESULT answer;
	char repairFile[50];
	HSCANRESULTS results = NULL;
	int numProblems = 0;
	SymantecVerdict verdictScan;

	retCode = ScanClientStreamStartEx(_scanClient, pOrgName, (char*)"", &hScanStream);

	LOG(Notification, "_doScanning - ScanClientStreamStartEx Return :", "%d", retCode);

	retCode = ScanClientStreamSendBytes(hScanStream, (LPBYTE)strContent.c_str(), strContent.length());

	LOG(Notification, "_doScanning - ScanClientStreamSendBytes Return :", "%d", retCode);

	retCode = ScanClientStreamFinish(hScanStream, repairFile, &results);

	LOG(Notification, "_doScanning - ScanClientStreamFinish Return :", "%d", retCode);


	switch (retCode)
	{
		case SCSCANFILE_CLEAN:
			verdictScan = SymantecVerdictOkey;
			break;
		case SCSCANFILE_INF_NO_REP:
			verdictScan = SymantecVerdictSuspectNoRepaired;
			break;
		case SCSCANFILE_INF_REPAIRED:
			verdictScan = SymantecVerdictSuspectRepaired;
			break;
		case SCSCANFILE_FAIL_ABORTED:
		case SCSCANFILE_INVALID_PARAM:
		case SCSCANFILE_FAIL_RECV_FILE:
		case SCSCANFILE_FAIL_MEMORY:
		case SCSCANFILE_FAIL_FILE_ACCESS:
		case SCSCANFILE_ERROR_SCANNINGFILE:
		case SCSCANFILE_ABORT_NO_AV_SCANNING_LICENSE:
			verdictScan = SymantecVerdictFailed;
			break;
		default:
			verdictScan = SymantecVerdictUnknown;
			break;
	}

	if (retCode > 0)
	{
		if (retCode == SCSCANFILE_FAIL_SERVER_TOO_BUSY)
		{
			LOG(Notification, "_doScanning - ScanClientStreamFinish Return :", "Error : Server too busy!");
		}
		else LOG(Notification, "_doScanning - ScanClientStreamFinish Return :", "Error : could not scan a file !");

		if (results)
			ScanResultsFree( results );
	
		return verdictScan;
	}

	if (results)
	{
		if ( ScanResultGetNumProblems( results, &numProblems ) > 0)
		{
			 LOG(Notification, "_doScanning - ScanResultGetNumProblems ", "Error ");
		}
		LOG(Notification, "_doScanning - ScanResultGetNumProblems Had Issue :", "%d", numProblems);
	}

	for(int i=0; i < numProblems; i++)
	{
		getScanProblemInfo(results, i, virusName);
	}

	// free scan result structure
	if (results)
		ScanResultsFree( results );

	return verdictScan;
}


void SymantecScanner::getScanProblemInfo(HSCANRESULTS hResults, int iWhichProblem, string virusName)
{
	char attrib[500];
	int attrib_size = 500;
	int iDisposition;
	char* ptrStrTrue = NULL;
	ptrStrTrue = (char*)"true";	


	ScanResultGetProblem( hResults,
							iWhichProblem,
							SC_PROBLEM_FILENAME,
							attrib,
							&attrib_size );

	LOG(Notification, "SymantecScanner::getScanProblem- ScanResultGetProblem, File Name -", "%s", attrib);

	attrib_size = 500;
	ScanResultGetProblem( hResults,
							iWhichProblem,
							SC_PROBLEM_VIRUSNAME,
							attrib,
							&attrib_size );
	LOG(Notification, "SymantecScanner::getScanProblem- ScanResultGetProblem, Virus Name:", "%s", attrib);

	attrib_size = 500;
	ScanResultGetProblem( hResults,
							iWhichProblem,
							SC_PROBLEM_NONVIRAL_THREAT_CATEGORY,
							attrib,
							&attrib_size );
	
	// set virus name
	virusName.assign(attrib);
	LOG(Notification, "SymantecScanner::getScanProblem- ScanResultGetProblem, Threat Category:", "%s", attrib);
}


//}	
