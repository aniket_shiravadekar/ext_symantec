#include "symantecapp.hxx"
#include "symantecscan.hxx"
#include "im_config2.h"

//namespace symantecscan 
//{

SymantecConfig* SymantecConfig::_pSymantecConfig = 0;

void SymantecApplication::initStat()
{
	//int errCode;
	LOG(Notification, "SymantecApplication", "In SymantecApplication::initStat");
	int	errCode;
	IM_Error imError;
	IM_FileInitArgs* fileInitArgs;
	IM_InitError(&imError);
	errCode = IM_InitFileInitArgs(&fileInitArgs, EXTN_SERVICE, &imError);
	IM_FreeError(&imError);
	
	//IM_InitError(&imError);
	//errCode = IM_InitStatFileWriter(&SymantecApplication::statHandle, MODULE_NAME, fileInitArgs, &imError);
	//IM_FreeError(&imError);
}

SymantecApplication::SymantecApplication(const char* prgName)
					:_bApplicationInitialized(false),
					_bSenderNotice(false),
					_bRecipientNotice(false),
					_bRemoteRecipientNotice(false),
					_bCOSAuthoritative(false),
					_strProgram(prgName),
					_strHostName(),
					_pSymantecConfig(NULL),
					_pSymantecScanner(NULL)
					
{
	LOG(Notification, "SymantecApplication", "In SymantecApplication::SymantecApplication");
	// Create disposition look up table
	_createDispHashTable();
	_createSymantecActionHashTable();
	//_createSymantecVirusNoticeHashTable();

	_strHostName = IM_GetMyHost();
}

SymantecApplication::~SymantecApplication()
{
	Finalize();
}

StringInfo* SymantecApplication::getDisposition(SymantecAction symAction)
{
	DispositionHash::iterator pDisp = _dispList.find(symAction);
	if ( pDisp != _dispList.end())
		return &pDisp->second;

	return NULL;
}

void SymantecApplication::_createSymantecActionHashTable()
{
	_actionList.clear();

	_actionList.insert(SymantecActionList::value_type("allow",    SymantecActionAllow));
	_actionList.insert(SymantecActionList::value_type("defer",    SymantecActionDefer));
	_actionList.insert(SymantecActionList::value_type("sideline", SymantecActionSideline));
	_actionList.insert(SymantecActionList::value_type("avspool",  SymantecActionAVSpool));
	_actionList.insert(SymantecActionList::value_type("reject",   SymantecActionReject));
	_actionList.insert(SymantecActionList::value_type("clean",    SymantecActionClean));
	_actionList.insert(SymantecActionList::value_type("repair",   SymantecActionRepair));
	_actionList.insert(SymantecActionList::value_type("discard",  SymantecActionDiscard));
}

SymantecAction SymantecApplication::getSymantecAction()
{
	LOG(Notification, "SymantecApplication::getSymantecAction", "Entry");
	string symAction(getSymantecInboundAction());
	SymantecActionList::iterator pAction = _actionList.find(symAction);

	if ( pAction == _actionList.end()) 
	{
		LOG(Error, "AVSymantecInvalidAction", "Action=%s", symAction.c_str());
		return SymantecActionInvalid;
	}

	switch ( pAction->second )
	{
		case SymantecActionAllow:
		case SymantecActionDefer:
		case SymantecActionSideline:
		case SymantecActionReject:
		case SymantecActionClean:
		case SymantecActionRepair:
		case SymantecActionDiscard:
			return pAction->second;

		case SymantecActionAVSpool:
		default:
			LOG(Error, "AVSymantecInvalidAction", "Action=%s", symAction.c_str());
			return SymantecActionInvalid;
	}
}
	

void SymantecApplication::Initialize()
{
	LOG(Notification, "SymantecApplication::Initialize", "Creating scanner");
	_pSymantecConfig = SymantecConfig::getInstance();
	_pSymantecScanner = new SymantecScanner();

	_bApplicationInitialized = _pSymantecScanner->isValid();
}


void SymantecApplication::Finalize()
{
	if (_pSymantecScanner)
		delete _pSymantecScanner;
	SymantecConfig::releaseInstance();
}

//+---------------------------------------------------------------------------
//
// Method:     SymantecApplication::_addDisp
//
// Synopsis:   Create an StringInfo object and assign disposition value and
//             insert into lookup table
//
//----------------------------------------------------------------------------
void  SymantecApplication::_addDisp(SymantecAction syAction, const char* disp)
{
    StringInfo strInfo;
    strInfo.str = disp;
    strInfo.len = int_cast(strlen(disp));

    _dispList.insert(DispositionHash::value_type(syAction, strInfo));
}

//+---------------------------------------------------------------------------
//
// Method:     SymantecApplication::_createDispHashTable
//
// Synopsis:   Create Disposition lookup table
//
//----------------------------------------------------------------------------
void  SymantecApplication::_createDispHashTable()
{
    _dispList.clear();

    _addDisp(SymantecActionInvalid,  "defer");
    _addDisp(SymantecActionDefer,    "defer");
    _addDisp(SymantecActionNull,     "keep");
    _addDisp(SymantecActionAllow,    "keep");
    _addDisp(SymantecActionSideline, "sideline");
    _addDisp(SymantecActionAVSpool,  "sideline avspool");
    _addDisp(SymantecActionReject,   "bounce This mail contains virus");
    _addDisp(SymantecActionDiscard,  "drop");
    _addDisp(SymantecActionClean,    "keep");
    _addDisp(SymantecActionRepair,   "keep");
}


//}
