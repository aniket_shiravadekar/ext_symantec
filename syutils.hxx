#ifndef __SYUTILS_HXX__
#define __SYUTILS_HXX__

//system includes 
#include <iostream>
#include <string>
#include <stdarg.h>
#include <vector>

//C-API includes
#include <im_log2.h>
#include <im_error.h>
#include <extapi.hxx>
#include <im_config2.h>
#include "syconstants.hxx"

using namespace std; 
//namespace symantecscan {

//+---------------------------------------------------------------------------
// Synopsis: This class is used as a wrapper class for Log and Trace apis 
//+---------------------------------------------------------------------------
class SymantecLog
{
public:
	// Constructor and Destructor
	SymantecLog(const char* name): _moduleName(name) {};
	~SymantecLog() {};

	void initSymantecLogging();
	void freeSymantecLogging();
	void setMainLogEvent(const char* logEvent, IM_MsgId msgId);

	// This function add log entry in the extension server log file
	static inline int writeLog(IM_LogSeverity severity, const char* logEvent,
								const char* logText="", ...)
	{
		if (symantecLogHandle == NULL)
			return -1;

		va_list ap;
		va_start(ap, logText);
		char message[INITIAL_LOG_MSG_SIZE];
		vsnprintf(message, sizeof(message), logText, ap);
	return IM_WriteLogArgs(symantecLogHandle, NULL, severity, logEvent, 1, message);
	}

	// This function add trace data in a log file if trace enabled
	static inline int writeTrace(const char* tag, short level, const char* format...)
	{
		if (!IM_IsTraceEnabled())
			return 0;	//return when trace not enabled

		va_list ap;
		va_start(ap, format);
		// 1024 is the maximum chars allowed for trace message
		char message[1024];
		vsnprintf(message, sizeof(message), format, ap);
		return IM_WriteTrace(symantecTraceHandle, NULL, tag, level, message);	 
	}

public:
	// log and trace handle
	static IM_FileInfo* symantecLogHandle;
	static IM_FileInfo* symantecTraceHandle;
	
private:
	// module name of the extension service
	string _moduleName;
};


class MxConfig;

typedef struct tagCfgContext
{
	int 		min;
	int 		max;
	IM_IMPACT 	impact;
	ConfigType	type;
	MxConfig	*parent;
} CfgContext;

// When error occured write the error message and error code to
// imextserv.trace.
bool HandleError(IM_Error& imError, const char* name="");

ImpactInfo* ConfigCallback(const char *host, const char* module, const char* key, IM_ACTION action,
		const char* value, void* callbackContext);


class MxConfig
{
public:
	MxConfig(){
		_registration = NULL;
	}
	MxConfig(const char* name, const char* defValue, 
		const char* host, const char* prog, ConfigType val, IM_IMPACT impact);
	//value_type=boolean
	MxConfig(const char* name, bool defValue,
        const char* host, const char* prog, ConfigType val, IM_IMPACT impact);
	//value_type=long
	MxConfig(const char* name, long defValue,
        const char* host, const char* prog, ConfigType val, IM_IMPACT impact);
	~MxConfig(){
		if(_registration)
			delete _registration;
	}

	inline SymantecAction getInboundAction() { return _inboundAction; }
	const char* getString(const char* key, const char* defVal);
	long getLong(const char* key, long defVal);
	bool getBool(const char* key, bool defVal);
	void addConfigKey(const char* program, const char* keyName, const char* value, 
				ConfigType type, IM_IMPACT impact, int min=0, int max=0);
	ImpactInfo*	getImpactInfo() { return _impactInfo; }

//private:
	void _setCfgSymantecInboundAction();

private:
	SymantecAction _inboundAction;	
	string _strInboundAction;
	
	void* _registration;
	CfgContext _callbackContext;	
	ImpactInfo* _impactInfo;	
	string _name;
};


typedef vector<std::string> StringArray;

class MxString : public std::string
{
  private:
    StringArray  _split(const MxString& delimiter);

  public:
    MxString(){};
    MxString(const char* str) : std::string(str) {};
    MxString(const char* str, long len) : std::string(str, len) {};
    MxString(const std::string& str) : std::string(str) {};
    MxString(char ch, int repeat);

    ~MxString(){};

    MxString& operator= (const std::string& str);
    MxString& operator= (const MxString& str);
    MxString& operator= (const char* str);
    MxString& operator= (const char& c);

    MxString& operator+= (const std::string& str);
    MxString& operator+= (const MxString& str);
    MxString& operator+= (const char* str);
    MxString& operator+= (const char& c); 

    bool operator== (const MxString& str);
    bool operator== (const std::string& str);
    bool operator== (const char* str);

    bool operator!= (const MxString& str);
    bool operator!= (const std::string& str);
    bool operator!= (const char* str);
    char operator[] (int index);

    MxString&    Trim();
    MxString     Clone(void) const;
    inline void  Clear(void) { erase(); };
    int          CompareNoCase(const char* str);
    int          Index(const char* str, int start=0) const;
    int          RIndex(const char* str, int start=0) const;
    bool         HasPrefix(const char* str) const;
    MxString&    UpperCase();
    MxString     Segment(int start, int length) const;
    StringArray  Split(const char* delimiter);
    MxString     S(const char* macro, const MxString& replace, const char* opts = "");
    MxString     S(const char* macro, const char* replace, const char* opts = "");
    char*        GetBuf() const;  // return the copy of the string buffer

    inline const char* AsPtr() const { return c_str(); }
    inline operator const char * () const { return (c_str() ? c_str() : ""); }

    inline friend int Length ( const MxString&);
    inline friend int Scalar ( const MxString&);
};

int Length(const MxString& str)
{
    return str.size();
}

int Scalar(const MxString& str)
{
    return str.size();
}

inline int int_cast(long n) 
{ 
#ifdef BUILDING_64

  bool negative = false;
  if (n < 0) {
    negative = true;
    n = -n;
  }

    // Treat n as array of int. 
    int * a = (int*) &n; 

    // Raise exception if we are about to truncate. 
    if(a[msb_index] != 0)
        REPORT_McAfeeInvalidValue(MLOGHANDLE, Error, "int_cast causes data truncation.");

    // Convert. 
    return negative ? -a[lsb_index] : a[lsb_index]; 
#else
    return n;
#endif
} 

/*
std::string IM_GetRFC822Date()
{
    return RFC822Date().AsPtr();
}
*/

#endif
