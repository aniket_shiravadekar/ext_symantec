#ifndef __SYMANTECHANDLER_HXX__
#define __SYMANTECHANDLER_HXX__

#include <iostream>
#include <vector>
#include <map>

#include "syconstants.hxx"
#include "symantecapp.hxx"
#include "msginfo.hxx"

using namespace std;

//namespace symantecscan
//{

class Handler
{
public:
	Handler(IMxMailDelivery* MailDelivery);
	~Handler();

	// Core functions
	SymantecCode Initialize(SymantecHookType hookType = SymantecInbound);
	SymantecCode PreProcess();
	SymantecCode Process();
	SymantecCode Commit();

	// Process on mail as per action defined
	void deferMessage();
	void takeBadMsgAction();
	void commitNoChange();
	//void commitTimeout();
	void commitResponse();
	bool generateNotice(const char* header, const char* body, bool sender=true);

	// Getters for data members
	MsgInfo* getMsgInfo() { return _msgInfo; }

public:
	typedef enum {
		ActionDefault = 0,
		ActionNewHeader = 1,
		ActionNewBody = 2,
		ActionNewHeaderAndBody = 4
	} ActionKey;

	typedef struct tagRecipInfo {
		StringInfo* rcptTo;
		StringInfo* rcptToDisp;
	} RecipInfo;

	typedef vector<RecipInfo*> RecipInfoArray;
	typedef vector<StringInfo*> RecipCosArray;

	typedef struct tagMailInfo {
		StringInfo* header;
		StringInfo* body;
		RecipInfoArray recipInfoArray;
	} MailInfo;

	typedef map<ActionKey, MailInfo> MailInfoArray;

private:
	void 		_postProcess();
	void 		_addVirusNoticeRecipInfo(RecipInfo* recipInfo, 
											Recipient* recip, SymantecAction symAction);
	void 		_addMailInfo(SymantecAction symAction, Recipient* recip=NULL);
	RecipInfo* 	_createRecipInfo(StringInfo* disp, Recipient* recip);
	void 		_cleanMainInfoArray();

private:

	int 				_recipCount;
	int					_virusNoticeCount;

	MsgInfo* 			_msgInfo;
	IMxMailDelivery* 	_mailDelivery;
	MailInfoArray 		_mailActionArray;
	RecipInfoArray		_virusNoticeRecipArray;
	RecipCosArray		_virusNoticeCosRecipArray;

	string 				_strVNoticeRecipients;
};

//}
#endif

