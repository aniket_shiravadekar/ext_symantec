#include "syutils.hxx"

//namespace symantecscan {

//+---------------------------------------------------------------------------
// SymantecLog class implementation
//----------------------------------------------------------------------------
IM_FileInfo* SymantecLog::symantecLogHandle = NULL;
IM_FileInfo* SymantecLog::symantecTraceHandle = NULL;

//+---------------------------------------------------------------------------
//
// Method:	SymantecLog::initSymantecLogging
//
// Synopsis: This function is called from Initialize(). It gives call to 
//			 IM_Initxxx() function and initializes symantecLogHandle and 
//			 symantecTraceHandle.
//	
//			 This function should get called only once when the service library
//			 is loaded, i.e. only in Initialize().	
//
//---------------------------------------------------------------------------
void SymantecLog::initSymantecLogging()
{
	int errCode;
	IM_Error imError;
	IM_FileInitArgs* fileInitArgs;
	IM_InitError(&imError);
	errCode = IM_InitFileInitArgs(&fileInitArgs, EXTN_SERVICE, &imError);
	IM_FreeError(&imError);

	IM_InitError(&imError);
	errCode = IM_InitLogFileWriter(&SymantecLog::symantecLogHandle, _moduleName.c_str(), fileInitArgs, &imError);
	IM_FreeError(&imError);

	IM_InitError(&imError);
	errCode = IM_InitTraceFileWriter(&SymantecLog::symantecTraceHandle, _moduleName.c_str(), fileInitArgs, &imError);
	IM_FreeError(&imError);
}

//+---------------------------------------------------------------------------
//
// Method:	SymantecLog::freeSymantecLogging
//
// Synopsis: This function is called from Finalize(). It gives call to 
//			 IM_Freexxx() function and free symantecLogHandle and 
//			 symantecTraceHandle.
//	
//			 This function should get called only once when the service library
//			 is unloaded, i.e. only in Finalize().	
//
//---------------------------------------------------------------------------
void SymantecLog::freeSymantecLogging()
{
	int errCode;
	IM_Error imError;

	IM_InitError(&imError);
	errCode = IM_FreeLogFileWriter(SymantecLog::symantecLogHandle, &imError);
	IM_FreeError(&imError);

	IM_InitError(&imError);
	errCode = IM_FreeTraceFileWriter(SymantecLog::symantecTraceHandle, &imError);
	IM_FreeError(&imError);
}

//+---------------------------------------------------------------------------
//
// Method:	SymantecLog::setMainLogEvent
//
// Synopsis: This function called from Initialize() to set the main log event.
//
//---------------------------------------------------------------------------
void SymantecLog::setMainLogEvent(const char* logEvent, IM_MsgId msgId)
{
	IM_Error imError;
	IM_InitError(&imError);

	// errors may occured related to arguments, error can ignore
	IM_SetLogEvent(SymantecLog::symantecLogHandle, logEvent, msgId, &imError);

	IM_FreeError(&imError); 
}

//+---------------------------------------------------------------------------
// MxConfig class implementation
//----------------------------------------------------------------------------

//value_type= string
MxConfig::MxConfig(const char* keyName, const char* defValue, const char* host, const char* prog, ConfigType val, IM_IMPACT impact)
{
	LOG(Notification, "MxConfig::MxConfig", "Entry");
	
	addConfigKey(EXTN_SERVICE, keyName, defValue, val, impact);
	
	LOG(Notification, "MxConfig::MxConfig", "Exit");
}

//value_type=boolean
MxConfig::MxConfig(const char* keyName, bool defValue, const char* host, const char* prog, ConfigType val, IM_IMPACT impact)
{
	LOG(Notification, "MxConfig", "MxConfig::MxConfig");
	addConfigKey(EXTN_SERVICE, keyName, defValue ? "true" : "false", val, impact);
}

//value type=long 
MxConfig::MxConfig(const char* keyName, long defValue, const char* host, const char* prog, ConfigType val, IM_IMPACT impact)
{
	LOG(Notification, "MxConfig", "MxConfig::MxConfig");
	char buf[1024];
    snprintf(buf, sizeof(buf), "%ld", defValue);
	addConfigKey(EXTN_SERVICE, keyName, buf, val, impact);
}

void MxConfig::addConfigKey(const char* program, const char* keyName, const char* defValue, 
							ConfigType type, IM_IMPACT impact, int min, int max)
{
	LOG(Notification, "MxConfig", "MxConfig::addConfigKey Before");
	memset(&_callbackContext, 0, sizeof(CfgContext));
	if (_impactInfo != NULL)
		delete _impactInfo;

	_impactInfo = new ImpactInfo;

	_callbackContext.min    = min;
	_callbackContext.max    = max;
	_callbackContext.impact = impact;
	_callbackContext.type   = type;
	_callbackContext.parent = this;

	IM_Error imError;
	IM_InitError (&imError);

	_registration = IM_AddConfigKeyWithMsg(IM_GetMyHost(), program, 
											keyName, defValue, &ConfigCallback, 
											(void*) &_callbackContext, &imError);

	if (!HandleError(imError, _name.c_str()))
	{
		_registration = NULL;
	}

	IM_FreeError(&imError);
	LOG(Notification, "MxConfig", "MxConfig::addConfigKey After");
}

const char* MxConfig::getString(const char* keyName, const char* defValue)
{
	LOG(Notification, "MxConfig::getString", "Entry");

	IM_Error imError;
	IM_InitError(&imError);
	string str = IM_GetConfigValue(_registration,
									IM_GetMyHost(),
									EXTN_SERVICE,
									keyName, 
									defValue,
									&imError);
	if ( HandleError(imError, keyName))
	{
	
	}
	IM_FreeError(&imError);	

	LOG(Notification, "MxConfig Return :", str.c_str());
	return str.c_str();
}

long MxConfig::getLong(const char* keyName, long defValue)
{
    LOG(Notification, "MxConfig", "MxConfig::getLong");

    IM_Error imError;
    IM_InitError(&imError);
    long newLong = IM_GetConfigValueAsInt(_registration,
                        					IM_GetMyHost(),
                        					EXTN_SERVICE,
                        					keyName, 
                        					defValue,   
                        					&imError);

    LOG(Notification, "MxConfig Return :", "%d", newLong);
    return newLong;
}

bool MxConfig::getBool(const char* keyName, bool defValue)
{
    LOG(Notification, "MxConfig", "MxConfig::getBool");

    IM_Error imError;
    IM_InitError(&imError);
    bool newBool = IM_GetConfigValueAsBool(_registration,
                        					IM_GetMyHost(),
                        					EXTN_SERVICE,
                        					keyName,
                        					defValue,
                        					&imError);

    LOG(Notification, "MxConfig Return :", newBool ? "true" : "false");
    return newBool;
}


ImpactInfo* ConfigCallback(const char* host, const char* module, const char* key, IM_ACTION action,
							const char* value, void* callbackContext)
{

	CfgContext* cfgContext = reinterpret_cast<CfgContext*> (callbackContext);
	ImpactInfo* impactInfo = cfgContext->parent->getImpactInfo();

	memset(impactInfo, 0, sizeof(ImpactInfo));

	if (action != IM_CFG_ASSESS)
	{
		DIAGC("symantecConfig", 3, "Impact=UPDATE, key=%s, module=%s, value=%s", key, module, value);
		//cfgContext->parent->dataUpdated();
		impactInfo->name = cfgContext->impact;
		return impactInfo;
	}

	DIAGC("symantecConfig", 3, "Impact=ASSESS, key=%s, module=%s, value=%s", key, module, value);
	
	switch(cfgContext->type)
	{
		case Cfg:
		default:
			break;
	}

	impactInfo->name = cfgContext->impact;
	return impactInfo;
}

bool HandleError(IM_Error& imError, const char* name)
{
	if (imError.number)
	{
		DIAGC("mcafee", 1,
				"Error in Config/Stat API. Name=%s, errorCode=%d, errorMsg=%s",
				name, imError.number, imError.string);
		return false;
	}

	return true;
}


MxString::MxString(char ch, int repeat)
{
    // check for sane argument value
    if (repeat < 0) repeat = 0;

    char* pc = new char [repeat];
    memset(pc, ch, repeat);
    assign(pc, repeat);
    delete [] pc;
}


MxString& MxString::operator=(const MxString& str)
{
    if ( &str != this)
        assign(str.begin(), str.end());
    return *this;
}
 
MxString& MxString::operator= (const std::string& str)
{
    if ( &str != this)
        assign(str.begin(), str.end());
    return *this;
}

MxString& MxString::operator= (const char* str)
{
    assign(str, str + strlen(str));
    return *this;
}
 
MxString& MxString::operator= (const char& c)
{
    assign(static_cast<size_type>(1), c); 
    return *this;
}
  
MxString& MxString::operator+= (const std::string& str)
{
    append(str);
    return *this;
}

MxString& MxString::operator+= (const MxString& str)
{
    append(str);
    return *this;
}

MxString& MxString::operator+= (const char* str)
{
    append(str);
    return *this;
}

MxString& MxString::operator+= (const char& c)
{
    push_back(c);
    return *this;
}
 
bool MxString::operator== (const MxString& str)
{
    return compare(str) == 0;
}


bool MxString::operator== (const std::string& str)
{
    return compare(str) == 0;
}


bool MxString::operator== (const char* str)
{
    return compare(str) == 0;
}

 
bool MxString::operator!= (const MxString& str)
{
    return compare(str) != 0;
}

bool MxString::operator!= (const std::string& str)
{
    return compare(str) != 0;
}


bool MxString::operator!= (const char* str)
{
    return compare(str) != 0;
}

 
char MxString::operator[](int index)
{
    if (index < 0 )  index += int_cast(size());

    if (index >= 0 && index < int_cast(size()))
        return data()[index];

    return 0;
}

int MxString::CompareNoCase(const char* str)
{
    return strncasecmp(c_str(), str, size()) == 0;
}
 
 
int MxString::Index(const char* str, int start) const
{
    return int_cast(find(str, start));
}
 
int MxString::RIndex(const char* str, int start) const
{
    if ( !str )
        return -1;

    if (start <= 0 || start > int_cast(size()))
        start = int_cast(size());

    return int_cast(rfind(str, start));
}

bool MxString::HasPrefix(const char* str) const
{
    bool result = false;
    if (str) {
        int buflen = int_cast(strlen(str));

        if (int_cast(this->length()) < buflen)
                return result;

        result = (strncmp(AsPtr(), str, buflen) == 0) ? true : false;
    }

    return result;
}

MxString MxString::Clone(void) const
{
    if (!empty()) {

        return MxString(c_str(), size());

    } else {

        return MxString();

    }
}

MxString& MxString::Trim(void)
{
    long spaces;

    //Remove whitespace from the beginning of the string.
    for (spaces = 0; spaces < size() && isspace(data()[spaces]); ++spaces)
        ;

    if (spaces > 0)
        erase(0, spaces);

    //Remove whitespace from the end of the string.
    for (spaces = 0; 
         spaces < size() && isspace(data()[size() - spaces - 1]); 
         ++spaces)
        ;

    if (spaces > 0)
        erase(size() - spaces, spaces);
    
    return *this;
}

MxString& MxString::UpperCase()
{
    std::transform(this->begin(), this->end(), this->begin(), 
                 (int(*)(int))(std::toupper));
    return *this;
 
}
 
MxString MxString::Segment(int start, int length) const
{
    return size() ? substr(start, length).c_str() : "aa";
}


StringArray MxString::Split(const char* delimiter)
{
    if (strlen(delimiter) > 1)
    {
        MxString strDelimiter(delimiter);
        return _split(strDelimiter);
    }

    StringArray  strList;
 
    char* tokptr; // Use by strtok_r to store it's state.
    
    char* buf = strdup(c_str());
   
    char* p; 
    p=strtok_r(buf, delimiter,&tokptr);
 
    while( p!=NULL)
    {
        if(p)
            strList.push_back(p);
 
        p=strtok_r(NULL, delimiter ,&tokptr);
    }
 
    free(buf);
    return strList;
}

// This function allocates memory and return a copy of the string buffer. 
// The caller should free the buffer.
char* MxString::GetBuf() const
{
    char* pc = new char[size()];
    if (pc == NULL) return NULL;

    memmove(pc, c_str(), size());
    return pc;
}

MxString MxString::S(const char* macro, const char* replace, const char* opts)
{
    MxString strReplace(replace);
    return S(macro, strReplace, opts);
}

// This is the special implementation to support mcafee virus Notices, 
// so only opts="g" is supported
MxString MxString::S(const char* macro, const MxString& replace, const char* opts)
{
	LOG(Notification, "In S()", "Entry");
	
    // only "g" option is supported/implemented.
    if ( strncmp(opts, "g",1) != 0)
        return *this;

    MxString strMacro = macro;
    StringArray strList = _split(strMacro);
    std::string tmpStr="";

    if (strList.size() == 0)
        return *this;

    for( StringArray::iterator ptr = strList.begin();
         ptr != strList.end(); ptr++)
    {
	if (tmpStr.size() == 0)
	{
            // Check if the "macro" is at the begining
            //of the string
            size_t nPos = find(macro,0);
            if (nPos == 0 )
                tmpStr = replace;
        }

	if ( ptr == strList.begin())
	    tmpStr += *ptr;
        else 
            tmpStr += replace + *ptr;
    }

    // Check if the "macro" is at the end of the string
    size_t nPos = find(macro, size() - strlen(macro));
    if (nPos != std::string::npos)
        tmpStr += replace;

    assign(tmpStr.begin(), tmpStr.end());

	LOG(Notification, "In S() Return->", "%s", this->AsPtr());
    return *this;
}

StringArray MxString::_split(const MxString& delimiter)
{
    std::string input = c_str();
    StringArray strList;
    strList.clear();

    long iPos = 0;
    int newPos = -1;
    size_t sizeS2 = delimiter.size();
    size_t isize = size();

    std::vector<int> positions;

    newPos = int_cast(input.find (delimiter, 0));

    if( newPos < 0 ) { return strList; }

    int numFound = 0;

    while( newPos >= iPos )
    {
        numFound++;
        positions.push_back(newPos);
        iPos = newPos;
        newPos = int_cast(input.find (delimiter, iPos+sizeS2+1));
    }

    for( int i=0; i <= int_cast(positions.size()); i++ )
    {
        std::string strTmp;
        if( i == 0 ) { strTmp = input.substr(i, (positions[i]) ); }

        int offset = positions[i-1] + int_cast(sizeS2);

        if( offset < int_cast(isize) )
        {
            if( i == int_cast(positions.size()) )
            {
                strTmp = input.substr(offset);
            }
            else if( i > 0 )
            {
                strTmp = input.substr(positions[i-1] + sizeS2, 
                         positions[i] - positions[i-1] - sizeS2 );
            }
        }
        if( strTmp.size() > 0 )
        {
            strList.push_back(strTmp);
        }
    }

    return strList;
}


//+---------------------------------------------------------------------------
// Helper function to join all the elements in the string with
// the seperator and return a MxString.
//+---------------------------------------------------------------------------
MxString Join(StringArray& strArray, const char* seperator)
{
    MxString newStr;

    if (strArray.empty())
        return newStr;

    StringArray::iterator p = strArray.begin();

    // initialize newStr with first element.
    newStr = *p;

    for( p++; p != strArray.end(); p++)
    {
        newStr += seperator;
	newStr += *p;
    }

    return newStr;
}
