#include "msginfo.hxx"

//namespace symantecscan
//{

MailUser::MailUser(IMxMailgram* Mailgram) : _mailgram(Mailgram),
					_envelope(NULL),
					_actionType(SymantecUnknown),
					_action(SymantecActionInvalid),
					_noticeStr(),
					_result(0)
{
	_result = _mailgram->getEnvelope(&_envelope);
	if( _result < 0 )
	{
		LOG(Error, "AVExtServerDataReadFailed", "Error reading Envelope information from extension server");
		throw(_result);
	}
	LOG(Notification, "MailUser::MailUser", "Exit");
}

MailUser::~MailUser()
{}

void MailUser::setSymantecAction(SymantecAction symAction)
{
	_action = symAction;
}

Recipient::Recipient(IMxMailgram* Mailgram, int index)
	: MailUser(Mailgram),
		_index(index),
		_bAddress(false),
		_bRemote(false)
{
}

Recipient::~Recipient()
{}

bool Recipient::isRemoteAddress()
{
	_bRemote = (getAddrStrInfo()->str[0] == '<') ? true : false;
	return _bRemote;
}

const StringInfo* Recipient::getAddrStrInfo()
{
	IMxMailAddress*   mailAddress;
	const StringInfo* strInfoAddress;

	_result = _envelope->getRcptTo(_index, &mailAddress);
	_result = mailAddress->getAddress( &strInfoAddress);

	return strInfoAddress;
}

const char* Recipient::getRcptToDisp()
{
	const StringInfo* infoRcptToDisp;
	_result = _mailgram->getRcptToDisp( _index, &infoRcptToDisp);

	if(!_result)
		return infoRcptToDisp->str;

	LOG(Notification, "Recipient::getRcptToDisp Return:", "%s", infoRcptToDisp->str);
	return NULL;
}

int Recipient::getDisposition(StringInfo** infoRcptToDisp)
{
	return _mailgram->getRcptToDisp( _index, (const StringInfo**) infoRcptToDisp);
}

const char* Recipient::getRcptToAttr()
{
	const StringInfo* infoRcptToAttr;
	_result = _mailgram->getRcptToAttr(_index, &infoRcptToAttr);
	if (!_result)
		return infoRcptToAttr->str;

	LOG(Notification, "Recipient::getRcptToAttr Return:", infoRcptToAttr->str); 
	return NULL;
}

SymantecAction Recipient::getSymantecAction()
{
	return _action;
}

//+---------------------------------------------------------------------------
//
// Method:     Recipient::getAddress
//
// Synopsis:   returns recipient's address as const char*
//
//----------------------------------------------------------------------------
const char* Recipient::getAddress()
{
  const StringInfo* addrInfo = getAddrStrInfo();

  if (addrInfo != NULL)
      return addrInfo->str;

  return "";
}


Sender::Sender(IMxMailgram* Mailgram)
		: MailUser(Mailgram)
{
}

Sender::~Sender()
{}

const StringInfo* Sender::getAddrStrInfo()
{
	
	IMxMailAddress* mailAddress;
	const StringInfo* strInfoAddress;

	_result = _envelope->getMailFrom( &mailAddress );
	_result = mailAddress->getAddress( &strInfoAddress );

	return strInfoAddress;
}

const char* Sender::getMailFromAttr()
{
	const StringInfo* infoMailFromAttr;
	_result = _mailgram->getMailFromAttr( &infoMailFromAttr);

	if(!_result)
		return infoMailFromAttr->str;
	LOG(Notification, "Sender::getMailFromAttr Return:", "%s", infoMailFromAttr->str);
}

SymantecAction Sender::getSymantecAction()
{
	return _action;
}

MsgInfo::MsgInfo(IMxMailDelivery* MailDelivery)
		:_result(0),
		_msgID(),
		_subject(),
		_mailDelivery(MailDelivery),
		_mailgram(NULL),
		_envelope(NULL),
		_mailMessage(NULL)
		
				
{
	LOG(Notification, "MsgInfo::MsgInfo", "Enter");

	// Clear recipient array
	_recipArray.clear();

	//_dateSubmitted = IM_GetRFC822Date();

	// Getting mailgram
	_result = _mailDelivery->getMailgram(&_mailgram);
	if ( _result < 0 )
	{
		LOG(Error, "AVExtServerDataReadFailed", "Error getting MailGram");
		throw(_result);
	}

	// Getting mail envelop
	_result = _mailgram->getEnvelope(&_envelope);
	if ( _result < 0 ) 
	{
		LOG(Error, "AVExtServerDataReadFailed", "Error getting Envelop");
		throw(_result);
	}

	_result = _envelope->getRcptToCount(&_rcptToCount);
	if ( _result < 0 )
	{
		LOG(Error,"AVExtServerDataReadFailed", "Error getting Recipient count");
		throw(_result);
	}

	_result = _mailgram->getMessage(&_mailMessage);
	if ( _result < 0 )
	{
		LOG(Error,"AVExtServerDataReadFailed", "Error getting Message");
		throw(_result);
	}

	// store sender
	_result = _storeSender();

	// store recipient
	_storeRecipients();

	setMessageID();
	
	_peerIP = readFromHeader("[", "]");

	_subject = readFromHeader("subject:", false);

	LOG(Notification, "MSGINFO SUB ->", "%s", _subject.c_str());
	
	LOG(Notification, "MSGINFO MailFrom ->", "%s", getMailFrom()->str);

	LOG(Notification, "MSGINFO RCPT List->", "%s", getRecipientList());

	//NEEDIMPL	
	LOG(Notification, "MsgInfo", "Exit");
}


MsgInfo::~MsgInfo()
{
	//NEEDIMPL	
	_cleanRecipArray();
	delete _MailSender;	
}


void MsgInfo::_cleanRecipArray()
{
	// loop through and delete all each object in the array
	for(int index=0; index < _rcptToCount; index++)
	{
		delete _recipArray[index];
	}

	_recipArray.clear();
}


const StringInfo* MsgInfo::getMailFrom()
{
	LOG(Notification, "MsgInfo::getMailFrom", "Entry");
	IMxMailAddress*	pMailFrom;
	const StringInfo* pInfoMailFrom;

	_result = _envelope->getMailFrom( &pMailFrom );
	
	pMailFrom->getAddress( &pInfoMailFrom );

	return pInfoMailFrom;
}


const StringInfo* MsgInfo::getBody()
{	
	const StringInfo* pInfoBody;
	IMxMessageBody* pBody;

	LOG(Notification, "MsgInfo::getBody", "Enter");

	_result = _mailMessage->getBody( &pBody );
	_result = pBody->getBody( &pInfoBody);

	
	LOG(Notification, "MsgInfo::getBody", "%s", pInfoBody->str);
	return pInfoBody;
}

const StringInfo* MsgInfo::getHeader()
{
	const StringInfo* pInfoHeader;
	IMxMessageHeader* pHeader;
	LOG(Notification, "MsgInfo::getHeader", "Enter");

	_result = _mailMessage->getHeader( &pHeader);
	_result = pHeader->getHeader( &pInfoHeader );
	LOG(Notification, "MsgInfo::getHeader", "%s", pInfoHeader->str);

	return pInfoHeader;
}

string MsgInfo::getSubject()
{
	return _subject;
}


void MsgInfo::setMessageID()
{
	//string msgID;
	IMxSmtpControlInfo* pIMxSmtpControlInfo;
	LOG(Notification, "MsgInfo::setMessageID", "Enter");

	// Get message id from smtp
	if ( _mailDelivery->getControlInfo(&pIMxSmtpControlInfo) != -1 ) {
		const StringInfo* msgID;

		pIMxSmtpControlInfo->getMsgID(&msgID);
			
		if (msgID != NULL) {
			_msgID.assign(msgID->str);
			LOG(Notification, "MsgInfo::setMessageID, NOT NULL, ID", "%s", _msgID.c_str());
			return;
		}
	}	

	//LOG(Notification, "setMessageID ID ->", "%s", msgID->str);
	LOG(Notification, "setMessageID ID ->", "%s",  _msgID.c_str());

	//NEEDIMP - get message id from mail header / envelop
	LOG(Notification, "MsgInfo::setMessageID", "Exit");
}

string MsgInfo::getMessageID()
{
	return _msgID;
}


SymantecCode MsgInfo::_storeSender(void)
{
	SymantecCode retCode = SymantecSuccess;
	LOG(Notification, "MsgInfo::_storeSender", "Entry");
	try {
		_MailSender = new Sender(_mailgram);
	} catch(...) {
		
		LOG(Error, "AVExtServerDataReadFailed", "Error creating Sender object");
		retCode = SymantecFailure;
		delete _MailSender;
		_MailSender = NULL;
	}

	LOG(Notification, "MsgInfo::_storeSender", "Exit");
	return retCode;
}

SymantecCode MsgInfo::_storeRecipients(void)
{
	LOG(Notification, "MsgInfo::_storeRecipients", "Entry");
	SymantecCode retCode = SymantecSuccess;

	for(unsigned int idx=0;  idx != _rcptToCount; ++idx)
	{
		Recipient* recip = NULL;
		
		try {
			recip = new Recipient( _mailgram, idx );
			_recipArray.push_back( recip );
			LOG(Notification, "storeRecipients", "push");
		} catch(...) {
			LOG(Error, "AVExtServerDataReadFailed", "Not able to create Recipient objects");
			retCode = SymantecFailure;
			if ( recip)
				delete recip;
			
			// clean up previous object
			_cleanRecipArray();
			break;
		}
	}

	LOG(Notification, "MsgInfo::_storeRecipients", "Exit");
	return retCode;
}

Recipient* MsgInfo::getRecipient(int index)
{
	LOG(Notification, "MsgInfo::getRecipient, ID->", "%d", index);
	if ( (_recipArray.size()) > index)
		return _recipArray[index];
	LOG(Notification, "MsgInfo::getRecipient, NO RETURN", "Exit");
	return NULL;
}


void MsgInfo::setVirusName(const char* name, bool scanSet)
{
	_virusname = name;
}


void MsgInfo::setSymantecVerdict(SymantecVerdict verdict, bool scanSet)
{
	_virusVerdict = verdict;
}

void MsgInfo::setSymantecActionType(SymantecActionBase symActionType, SymantecAction symAction)
{
	_symantecAction = symAction;
	_symantecActionType = symActionType;	
}


const char* MsgInfo::getRecipientList()
{
	// Build recipient list if requested
	if ( Length(_recipientList) == 0)
	{
		for (int idx=0; idx != _rcptToCount; ++idx)
		{
			MxString strNoAngleRecipient(getRecipient(idx)->getAddress());
			if(idx) {
				_recipientList += strNoAngleRecipient;
			}
			_recipientList += strNoAngleRecipient;
		}
		LOG(Notification, "MsgInfo::getRecipientList", "%s", _recipientList.AsPtr());
	}

	return _recipientList;
}

MxString MsgInfo::readFromHeader(const char* cStart, const char* cEnd, int length)
{
	MxString strHeader(getHeader()->str);
	MxString newStrHeader;

	if ( length ==0 || length > Length(strHeader))
		newStrHeader = strHeader;
	else
		newStrHeader = strHeader.Segment(0, length);

	return _searchSString(newStrHeader, cStart, cEnd);
}


MxString MsgInfo::readFromHeader(const char* fldname, bool decode, bool perl)
{
	char* start = (char*)strstr(getHeader()->str, fldname);
	
	if (!start)
		return MxString();

	start += strlen(fldname);

	MxString line;
	char* end;

	do {
		end = strchr(start, '\n');
		
		if (!end)
			end = start + strlen(start);
		
		line += MxString(start, end - start).Trim();
		start = end + 1;

	} while (*end && *start && isspace(*start));

	if (decode) {
		//line = decodeMultiLines(line, perl);
	}
	else {
		line = escape(line, perl);
	}
	return line;
}

//+---------------------------------------------------------------------------
//
// Method:     MsgInfo::_searchSString
//
// Synopsis:   returns the string between start and end tokens
//
// Inputs:     Start token and end token
//
//----------------------------------------------------------------------------
MxString MsgInfo::_searchSString(const MxString& str, const char* cStart, const char* cEnd)
{
    MxString newStr;
    int iStart, iEnd;
    if ((iStart = str.Index(cStart)) >= 0 &&
        (iEnd = str.Index(cEnd, iStart)) >= 0 ) {

         iStart = iStart + int_cast(strlen(cStart)) - 1;
         newStr = str.Segment(iStart, iEnd - iStart + 1);
    }

    return newStr;
}


//}
//+---------------------------------------------------------------------------
//
// Method:     escape
//
// Synopsis:   optionally escape all '$; or '\\' chars
//             in input/output string
//
//             line: reference to string to escape
//             perl: option to escape $ or \\ chars,
//                   as required by perl
//----------------------------------------------------------------------------
MxString escape(const MxString& line, bool perl)
{
    const char* sub = line.AsPtr();
    MxString value;
    while (*sub) {
        if (perl && *sub == '$')
            value += '$';
        if (perl && *sub == '\\')
            value += '\\';
        value += *(sub++);
    }
    return value;
}
